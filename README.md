# K8s

apt-get update && apt-get install -y apt-transport-https

apt install docker.io -y

systemctl start docker && systemctl enable docker 

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

vi /etc/apt/sources.list.d/kubernetes.list

deb http://apt.kubernetes.io/ kubernetes-xenial main

apt-get update

apt-get install -y kubelet kubeadm kubectl kubernetes-cni

ifconfig

kubeadm init --apiserver-advertise-address=172.31.26.24

mkdir -p $HOME/.kube

cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://docs.projectcalico.org/v3.4/getting-started/kubernetes/installation/hosted/calico.yaml

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml

kubectl proxy --address 0.0.0.0 --accept-hosts '.*' &

Dashboard URL:-

http://<ip>:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=default


# Create service account to access the dashboard

kubectl create serviceaccount cluster-admin-dashboard-sa

# Bind ClusterAdmin role to the service account

kubectl create clusterrolebinding cluster-admin-dashboard-sa --clusterrole=cluster-admin --serviceaccount=default:cluster-admin-dashboard-sa

# Parse the token

TOKEN=$(kubectl describe secret $(kubectl -n kube-system get secret | awk '/^cluster-admin-dashboard-sa-token-/{print $1}') | awk '$1=="token:"{print $2}')

              
$ kubectl -n kube-system get service kubernetes-dashboard


#######################################################

## steps for kube worker node:

apt-get update && apt-get install -y apt-transport-https

apt install docker.io -y

systemctl start docker && systemctl enable docker

curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.7.0/bin/linux/amd64/kubectl

chmod +x ./kubectl

mv ./kubectl /usr/local/bin/kubectl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

vi /etc/apt/sources.list.d/kubernetes.list

deb http://apt.kubernetes.io/ kubernetes-xenial main

apt-get update

apt-get install -y kubelet kubeadm  kubernetes-cni

kubeadm join 172.31.26.24:6443 --token 






